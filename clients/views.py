from django.shortcuts import render

from rest_framework import viewsets, mixins, status
from rest_framework_api_key.models import APIKey
from rest_framework_api_key.permissions import HasAPIKey
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import ClientSerializer
from .models import Client


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    permission_classes = [HasAPIKey]

    def get_queryset(self):
        queryset = Client.objects.all()
        return queryset.order_by('-created_date')


@api_view(['POST'])
def get_apikey(request):
    client_name = request.data.get('name')
    if client_name == None or len(client_name) == 0:
        return Response({'error': 'O campo \"nome\" é obrigatório.'}, status=status.HTTP_400_BAD_REQUEST)
    client, created = Client.objects.get_or_create(name=client_name)
    
    if created:
        api_key, key = APIKey.objects.create_key(name=client_name)
        return Response({
            'api_key': key
        })
    else:
        return Response({'error': 'Já existe cliente com esse nome!.'}, status=status.HTTP_400_BAD_REQUEST)
