from django.urls import path, include

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('', views.ClientViewSet, basename='clients')

urlpatterns = [
    path('api_key/', views.get_apikey),
    path('', include(router.urls)),
]

