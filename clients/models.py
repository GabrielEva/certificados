from django.db import models

from tools.models import BaseClass


class Client(BaseClass):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name
