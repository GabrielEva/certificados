from django.test import TestCase, Client as RequestClient

from ..models import Client


class NewClientTestCase(TestCase):
    def setUp(self):
        pass

    def test_post_api_key(self):
        """ClientModel instance successfly created and api_key generated"""
        c = RequestClient()
        response = c.post('/clients/api_key/', {}, content_type='application/json')
        self.assertEqual(response.status_code, 400)

        response = c.post('/clients/api_key/', {'name': None}, content_type='application/json')
        self.assertEqual(response.status_code, 400)

        response = c.post('/clients/api_key/', {'name': ''}, content_type='application/json')
        self.assertEqual(response.status_code, 400)

        response = c.post('/clients/api_key/', {'name': 'Test'}, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual('api_key' in response.json(), True)

        test_client = Client.objects.get(name='Test')
        self.assertEqual(test_client != None, True)

        response = c.post('/clients/api_key/', {'name': 'Test'}, content_type='application/json')
        self.assertEqual(response.status_code, 400)

