from rest_framework import serializers

from .models import Certificate, CertificateTemplate
from clients.models import Client


class CertificateTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CertificateTemplate
        fields = '__all__'


class CertificateSerializer(serializers.ModelSerializer):
    client = serializers.SlugRelatedField(
        slug_field='name',
        queryset=Client.objects.all().order_by('-created_date')
    )
    class Meta:
        model = Certificate
        fields = ('client', 'template', 'content_title', 'content_time', 'content_organizer_name', 'content_organizer_signature', 'recipient_name', 'recipient_email')
