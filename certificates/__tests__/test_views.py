from django.test import TestCase, Client as RequestClient

from certificates.models import Certificate, CertificateTemplate
from ..models import Client


class CertificateTemplateListTestCase(TestCase):
    def setUp(self):
        test_template = CertificateTemplate.objects.create(static_url="test/")
        test2_template = CertificateTemplate.objects.create(static_url="test2/")
        test3_template = CertificateTemplate.objects.create(static_url="test3/")
        test4_template = CertificateTemplate.objects.create(static_url="test4/")
        test5_template = CertificateTemplate.objects.create(static_url="test5/")
        test6_template = CertificateTemplate.objects.create(static_url="test6/")

    def test_template_list(self):
        """Client can successfly get a template list"""

        c = RequestClient()
        response = c.post('/clients/api_key/', {'name': 'Test'}, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        api_key = response.json().get('api_key')

        response = c.get('/certificates/templates/')
        self.assertEqual(response.status_code, 403)

        response = c.get('/certificates/templates/', HTTP_AUTHORIZATION=('Api-Key %s' % api_key))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json().get('results')), 6)


class CertificatePostTestCase(TestCase):
    def setUp(self):
        test_template = CertificateTemplate.objects.create(static_url="test/")

    def test_create_certificate(self):
        """Client can successfly create a certificate and render it"""

        c = RequestClient()
        test_client_name = "Test"
        response = c.post('/clients/api_key/', {'name': test_client_name}, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        api_key = response.json().get('api_key')

        response = c.get('/certificates/templates/', HTTP_AUTHORIZATION=('Api-Key %s' % api_key))
        self.assertEqual(response.status_code, 200)
        results = response.json().get('results')
        certificate_template = results[0]

        response = c.post('/certificates/')
        self.assertEqual(response.status_code, 403)
        
        response = c.post(
            '/certificates/',
            {
                'client': test_client_name,
                'template': certificate_template.get('id'),
                'content_title': 'test',
                'content_time': 128,
                'content_organizer_name': 'test_organizer',
                # 'content_organizer_signature':
                'recipient_name': 'test_recipient',
                'recipient_email': 'test_recipient_email'
            },
            HTTP_AUTHORIZATION=('Api-Key %s' % api_key)
        )
        self.assertEqual(response.status_code, 201)

        test_client = Client.objects.get(name=test_client_name)
        test_template = CertificateTemplate.objects.get(pk=certificate_template.get('id'))
        test_certificate = Certificate.objects.get(client=test_client, template=test_template)
        
        self.assertEqual(test_certificate.content_title, 'test')
        self.assertEqual(test_certificate.content_time, 128)
        self.assertEqual(test_certificate.content_organizer_name, 'test_organizer')
        # self.assertEqual(test_certificate.content_organizer_signature,  )
        self.assertEqual(test_certificate.recipient_name, 'test_recipient')
        self.assertEqual(test_certificate.recipient_email, 'test_recipient_email')


class CertificateRenderTestCase(TestCase):
    def setUp(self):
        test_client = Client.objects.create(name="test")
        Certificate.objects.create(
            client=test_client,
            content_title="test",
            content_time=128,
            content_organizer_name="test_organizer",
            recipient_name="test_recipient",
            recipient_email="test_recipient_email"
        )

        test2_client = Client.objects.create(name="test2")
        test2_template = CertificateTemplate.objects.create(static_url="modulo01_organicos.html")
        Certificate.objects.create(
            client=test2_client,
            template=test2_template,
            content_title="test2",
            content_time=30,
            content_organizer_name="test2_organizer",
            recipient_name="test2_recipient",
            recipient_email="test2_recipient_email"
        )

    def test_render_certificate(self):
        """Client can successfly render a certificate"""

        c = RequestClient()
        test_client = Client.objects.get(name="test")
        test_certificate = Certificate.objects.get(client=test_client)
        response = c.get('/certificates/render/%s/' % test_certificate.key)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'default_certificate.html')

        test2_client = Client.objects.get(name="test2")
        test2_certificate = Certificate.objects.get(client=test2_client)
        response = c.get('/certificates/render/%s/' % test2_certificate.key)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.templates[0].name, 'modulo01_organicos.html')
