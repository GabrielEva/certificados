from django.test import TestCase

from ..models import Certificate, CertificateTemplate
from clients.models import Client


class CertificateCreationTestCase(TestCase):
    def setUp(self):
        test_client = Client.objects.create(name="test")
        Certificate.objects.create(
            client=test_client,
            content_title="test",
            content_time=128,
            content_organizer_name="test_organizer",
            # content_organizer_signature=
            recipient_name="test_recipient",
            recipient_email="test_recipient_email"
        )

        test2_client = Client.objects.create(name="test2")
        test2_template = CertificateTemplate.objects.create(static_url="test2/")
        Certificate.objects.create(
            client=test2_client,
            template=test2_template,
            content_title="test2",
            content_time=30,
            content_organizer_name="test2_organizer",
            # content_organizer_signature=
            recipient_name="test2_recipient",
            recipient_email="test2_recipient_email"
        )

    def test_creation(self):
        """Models are created successfully"""

        test_client = Client.objects.get(name="test")
        test_certificate = Certificate.objects.get(client=test_client)
        self.assertEqual(test_certificate.content_title, "test")
        self.assertEqual(test_certificate.content_time, 128)
        self.assertEqual(test_certificate.get_time_with_label(), "2 horas")
        self.assertEqual(test_certificate.content_organizer_name, "test_organizer")
        # self.assertEqual(test_certificate.content_organizer_signature, )
        self.assertEqual(test_certificate.recipient_name, "test_recipient")
        self.assertEqual(test_certificate.recipient_email, "test_recipient_email")

        test2_client = Client.objects.get(name="test2")
        test2_template = CertificateTemplate.objects.get(static_url="test2/")
        test2_certificate = Certificate.objects.get(client=test2_client, template=test2_template)
        
        self.assertEqual(test2_certificate.content_title, "test2")
        self.assertEqual(test2_certificate.content_time, 30)
        self.assertEqual(test2_certificate.get_time_with_label(), "30 minutos")
        self.assertEqual(test2_certificate.content_organizer_name, "test2_organizer")
        # self.assertEqual(test2_certificate.content_organizer_signature,  )
        self.assertEqual(test2_certificate.recipient_name, "test2_recipient")
        self.assertEqual(test2_certificate.recipient_email, "test2_recipient_email")
