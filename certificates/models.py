from django.db import models

import uuid

from tools.models import BaseClass
from clients.models import Client


class CertificateTemplate(BaseClass):
    static_url = models.CharField(max_length=2048)


class Certificate(BaseClass):
    key = models.CharField(max_length=32, unique=True, blank=True)
    
    client = models.ForeignKey(Client, related_name='certificates', on_delete=models.CASCADE)
    template = models.ForeignKey(CertificateTemplate, related_name="certificates", on_delete=models.SET_NULL, null=True, blank=True)

    content_title = models.CharField(max_length=500)
    content_time = models.FloatField()
    
    content_organizer_name = models.CharField(max_length=100)
    content_organizer_signature = models.ImageField(upload_to='certificates/organizer_signature', null=True, blank=True)
    
    recipient_name = models.CharField(max_length=100)
    recipient_email = models.CharField(max_length=256)
    
    def __str__(self):
        return '%s - %s' % (self.recipient_name, self.content_title)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = uuid.uuid4().hex
        super(Certificate, self).save(*args, **kwargs)

    def get_hours(self):
        return self.content_time/60

    def get_time_with_label(self):
        if self.content_time > 60:
            return '%d horas' % int(self.get_hours())
        return '%d minutos' % int(self.content_time)
