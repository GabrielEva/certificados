from django.contrib import admin

from .models import Certificate, CertificateTemplate

admin.site.register(Certificate)
admin.site.register(CertificateTemplate)
