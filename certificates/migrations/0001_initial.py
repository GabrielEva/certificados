# Generated by Django 3.1.7 on 2021-03-19 17:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CertificateTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('static_url', models.CharField(max_length=2048)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('key', models.CharField(blank=True, max_length=32, unique=True)),
                ('content_title', models.CharField(max_length=500)),
                ('content_time', models.FloatField()),
                ('content_organizer_name', models.CharField(max_length=100)),
                ('content_organizer_signature', models.ImageField(blank=True, null=True, upload_to='certificates/organizer_signature')),
                ('recipient_name', models.CharField(max_length=100)),
                ('recipient_email', models.CharField(max_length=256)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='certificates', to='clients.client')),
                ('template', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='certificates', to='certificates.certificatetemplate')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
