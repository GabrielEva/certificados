from django.shortcuts import render, get_object_or_404

from rest_framework import viewsets, mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework_api_key.permissions import HasAPIKey

from .serializers import CertificateSerializer, CertificateTemplateSerializer
from .filters import CertificateFilterBackend
from .models import Certificate, CertificateTemplate


class CertificateTemplateViewSet(viewsets.ModelViewSet):
    serializer_class = CertificateTemplateSerializer
    queryset = CertificateTemplate.objects.all().order_by('-created_date')
    permission_classes = [HasAPIKey]


class CertificateListViewSet(viewsets.ModelViewSet):
    serializer_class = CertificateSerializer
    filterset_class = CertificateFilterBackend
    permission_classes = [HasAPIKey]

    def get_queryset(self):
        queryset = Certificate.objects.all()
        return queryset.order_by('-created_date')


@permission_classes([AllowAny])
def render_certificate(request, key):
    certificate = get_object_or_404(Certificate, key=key)
    if (certificate.template):
        return render(request, certificate.template.static_url, {'certificate': certificate})
    return render(request, 'default_certificate.html', {'certificate': certificate})
