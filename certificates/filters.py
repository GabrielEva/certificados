from django_filters.rest_framework import FilterSet

from .models import Certificate


class CertificateFilterBackend(FilterSet):
    class Meta:
        model = Certificate
        fields = ['key', 'recipient_email']
