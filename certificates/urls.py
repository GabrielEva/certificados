from django.urls import path, include

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('templates', views.CertificateTemplateViewSet, basename='templates')
router.register('', views.CertificateListViewSet, basename='certificates')

urlpatterns = [
    path('render/<key>/', views.render_certificate),
    path('', include(router.urls)),
]

